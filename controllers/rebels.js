'use strict'

const logger = require('../config/logger');
const fs = require('fs');
const Rebel = require('../class/rebel');
const exceptions = require('../exceptions/exceptions');

const saveRebel = require('../functions/functions');

let rebel = new Rebel('','');

function getRebels(req, res){
    logger.info('enter function getRebels');
    try {
        fs.readFile('empire.json', (err, data) => {
            if(err) throw err
            var arrJSON = JSON.parse(data);
            res.status(200).send({empire: arrJSON.rebels});
        });
    } catch (error) {
        logger.error(error);
        res.status(404).send({message: 'Page not found'});
    }
   logger.info('end function getRebels');
}

function addRebel(req, res){
    logger.info('Enter function addRebel');
    let name = req.body.name;
    let planet = req.body.planet;
    
    saveRebel(name, planet);
    
    logger.info('end function addRebels');
}

module.exports = {
    getRebels,
    addRebel
};