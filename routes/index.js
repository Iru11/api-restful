'use strict'

const express = require('express');
const rebelsCtrl = require('../controllers/rebels')
const api = express.Router();

api.get('/empire/rebels', rebelsCtrl.getRebels);
api.post('/empire', rebelsCtrl.addRebel);

module.exports = api