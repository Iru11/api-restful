class Rebel{
    constructor(name, planet){
        this.name = name;
        this.planet = planet;
        this.date = new Date();
    }
}

module.exports = Rebel;