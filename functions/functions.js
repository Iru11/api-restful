'use strict'

const logger = require('../config/logger');
const exceptions = require('../exceptions/exceptions');
const Rebel = require('../class/rebel');
const fs = require('fs');

function saveRebel(name, planet){
    logger.info('Enter function saveRebel');
    let rebel = new Rebel('','');

    if(name == ""){
        throw new exceptions.RebelError('Name is empty!');
    }
    if(planet == ""){
        throw new exceptions.RebelError('Planet is empty!');
    }

    try {
        fs.readFile('empire.json', (err, data) => {
           if(err) throw err
           let arrJSON = JSON.parse(data);
           
            logger.warn(arrJSON.rebels.hasOwnProperty(0));

           if(!arrJSON.rebels.hasOwnProperty(0)){
            logger.warn(arrJSON.rebels.hasOwnProperty(0));
            rebel = new Rebel(name, planet);
            arrJSON.rebels.push(rebel);
           }else{ 
            logger.info(arrJSON.rebels);
            arrJSON.rebels.forEach(element => {
                
                if(element.name != name && element.planet != planet){
                    rebel = new Rebel(name, planet);
                }else{
                    throw new exceptions.RebelError('Rebel exists!');
                }
            });
            arrJSON.rebels.push(rebel);
           }
            
            fs.writeFile('empire.json', JSON.stringify(arrJSON, null, 4), 'utf-8', (err) =>{
                if(err) throw err
                logger.info('Rebel registered!');
                return true;
            });
            
        });

        res.status(200).send({message: 'post'});
    }catch (err) {
        if(err instanceof RebelExistError){
            logger.error(err);
            res.send({message: err.message})
        }
        logger.error(err);
        res.status(500).send({message: 'Invalid Field!'});
        res.status(404).send({message: 'Page not found!'})
    }
    logger.info('end function saveRebel');
}

module.exports = saveRebel;