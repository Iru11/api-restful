 function ExistsRebelException(message){
    this.message = message;
}

class RebelError extends Error{
    constructor(message){
        super(message);
        this.name = "RebelError";
    }
}

module.exports = {
    ExistsRebelException,
    RebelError
} 