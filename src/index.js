'use strict'
const logger = require('../config/logger');
const app = require('../app');
const config = require('../config/config');

app.listen(config.port, () =>{
    logger.info(`Server on port https://localhost:${config.port}`);
});